using UnityEditor;
using UnityEngine;

namespace Atherox.Editor
{
	[CustomPropertyDrawer(typeof(ExposeSOAttribute))]
	public class ExposedSOAttributeDrawer : PropertyDrawer
	{
		private UnityEditor.Editor mEditor;

		public override void OnGUI(Rect aPosition, SerializedProperty aProperty, GUIContent aLabel)
		{
			EditorGUI.PropertyField(aPosition, aProperty, aLabel, true);

			if (aProperty.objectReferenceValue != null)
			{
				aProperty.isExpanded = EditorGUI.Foldout(aPosition, aProperty.isExpanded, GUIContent.none);
			}

			if (aProperty.isExpanded)
			{
				++EditorGUI.indentLevel;

				if (!mEditor) UnityEditor.Editor.CreateCachedEditor(aProperty.objectReferenceValue, null, ref mEditor);

				mEditor.OnInspectorGUI();

				--EditorGUI.indentLevel;
			}
		}
	}
}