using Ath.Saves;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SaveSystem : MonoBehaviour
{
    public static SaveSystem Instance { get; private set; }

    private string savePath;
    private List<ISaveableObject> saveObjects = new();
    
    private static readonly double SAVE_VERSION = 1.1;

    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
            return;
        }
        Instance = this;

        savePath = Path.Combine(Application.persistentDataPath, "save.dat");
    }

    public void RegisterSaveable(ISaveableObject saveable)
    {
        saveObjects.Add(saveable);
    }

    public void RemoveSaveable(ISaveableObject saveable)
    {
        saveObjects.Remove(saveable);
    }

    public void SaveGameData()
    {
        Debug.Log("[SaveSystem] - Starting to Save");

        SaveData saveData = new SaveData
        {
            saveVersion = SAVE_VERSION,
            sceneName = SceneManager.GetActiveScene().name,
            objs = saveObjects.Select(obj => obj.SaveData()).ToList()
        };

        try
        {
            string jsonData = JsonConvert.SerializeObject(saveData, Formatting.None, 
                new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
            File.WriteAllText(savePath, jsonData);
            Debug.Log("[SaveSystem] - Finished Saving");
        }
        catch(Exception e)
        {
            Debug.LogError($"[SaveSystem] - Error while trying to save {savePath} with exception {e}");
        }
    }

    public async Task LoadGameData()
    {
        if (!File.Exists(savePath))
        {
            Debug.LogError("[SaveSystem] - No save data");
            return;
        }
        Debug.Log("[SaveSystem] - Starting to Load");

        string jsonData = File.ReadAllText(savePath);
        SaveData loadData = JsonConvert.DeserializeObject<SaveData>(jsonData);

        if (loadData.saveVersion != SAVE_VERSION)
        {
            Debug.LogError($"[SaveSystem] - Save version {loadData.saveVersion} doesn't match with current game version {SAVE_VERSION}");
            return;
        }
        
        if (loadData.sceneName != SceneManager.GetActiveScene().name)
        {
            Debug.Log($"[SaveSystem] - Loading Scene {loadData.sceneName}");
            await LoadSceneAndDataAsync(loadData);
        }
        else
        {
            LoadJsonData(loadData);
        }
    }

    private void LoadJsonData(SaveData loadData)
    {
        foreach (var objectData in loadData.objs)
        {
            GameObject prefab = Resources.Load<GameObject>(objectData.prefabPath);

            if(prefab is null)
            {
                Debug.LogError($"[SaveSystem] - Prefab in {objectData.prefabPath} not found!");
                continue;
            }
            
            GameObject existingObject = GameObject.Find(objectData.objectID);
            if(existingObject is not null)
            {
                existingObject.TryGetComponent(out ISaveableObject loadedObject);
                if (loadedObject != null)
                {
                    loadedObject.Load(objectData);
                }
                else
                {
                    Debug.LogError($"[SaveSystem] - No SaveableObject found on {existingObject.name}.");
                }
            }
            else
            {
                GameObject newObject = Instantiate(prefab);
                newObject.name = objectData.objectID; // Asignamos el mismo nombre o ID
                newObject.TryGetComponent(out ISaveableObject loadedObject);
                
                if(loadedObject != null)
                {
                    loadedObject.Load(objectData);
                }
                else
                {
                    Debug.LogError($"[SaveSystem] - Why is {prefab.name} here??");
                }
            }
        }
    }

    private async Task LoadSceneAndDataAsync(SaveData loadData)
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(loadData.sceneName);
        asyncLoad.completed += asyncLoad => LoadJsonData(loadData);
        while (!asyncLoad.isDone)
        {
            return;
        }
    }
}
