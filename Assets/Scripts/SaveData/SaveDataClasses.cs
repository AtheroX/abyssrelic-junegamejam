using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

namespace Ath.Saves
{
    public interface ISaveableObject
    {
        SaveableObjectData SaveData();
        void Load(SaveableObjectData saveableObject);
    }

    [Serializable]
    public class SaveableObjectData
    {
        public string prefabPath;
        public bool isActive;
        public Dictionary<string, object> data = new();
        public string objectID;
    }

    [Serializable]
    public class SaveData
    {
        public double saveVersion;
        [FormerlySerializedAs("SceneName")] public string sceneName;
        public List<SaveableObjectData> objs = new();
    }
}