using UnityEngine;

namespace Atherox.Editor
{
	[System.AttributeUsage(System.AttributeTargets.Field, AllowMultiple = true)]
	public class ExposeSOAttribute : PropertyAttribute
	{
	}
}