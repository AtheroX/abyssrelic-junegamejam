using System;
using Ath.Saves;
using Atherox.Editor;
using Atherox.Player.Info;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.Serialization;

namespace Atherox.Player
{
	/**
	 * Class that handles the controller of the player.
	 */
	[RequireComponent(typeof(Rigidbody2D), typeof(Collider2D))]
	public class PlayerController : MonoBehaviour, IPlayerController, ISaveableObject
	{
		[ExposeSO] [SerializeField] private PlayerControllerSO mStats;
		private Rigidbody2D mRB;
		private CapsuleCollider2D mCollider;
		private FrameInput mFrameInput;
		private Vector2 mFrameVelocity;
		private bool mCachedQueryStartsInColliders;

		private float mTime;
		private Camera mCamera;

        #region Interface

		public FrameInput FrameInput => mFrameInput;
		public event Action<bool, float> GroundedChanged;
		public event Action Jumped;
		public event Action<int> WallJumped;
		public event Action<Vector2> GrapplingShooted;
		public event Action Ungrapple;

        #endregion

        #region Collisions

		private float mFrameLeftGround;
		private bool mGrounded;
		private bool mTouchingWall;
		private int mWallDirection;

        #endregion

        #region Jumping

		private bool mJumpToConsume;
		private bool mBufferedUsableJump;
		private bool mEndedJumpEarly;
		private bool mCoyoteUsable;
		private float mTimeJumpWasPressed;

		private bool HasBufferedJump => mBufferedUsableJump && mTime < mTimeJumpWasPressed + mStats.JumpBuffer;
		private bool CanUseCoyote => mCoyoteUsable && !mGrounded && mTime < mFrameLeftGround + mStats.CoyoteTime;

        #endregion

        #region Grapple

		private bool mGrappleToConsume;
		private bool mToUngrapple;
		private bool mGrabbed;

        #endregion

        #region SaveSystem
        public SaveableObjectData SaveData()
        {
            SaveableObjectData saveData = new SaveableObjectData
            {
                prefabPath = "Prefabs/PlayerController",
                isActive = gameObject.activeSelf,
                objectID = gameObject.name
            };

            saveData.data.Add("position", transform.position);
            saveData.data.Add("rotation", transform.rotation.eulerAngles);
            saveData.data.Add("mFrameInput", mFrameInput);
            saveData.data.Add("mJumpToConsume", mJumpToConsume);
            saveData.data.Add("mTimeJumpWasPressed", mTimeJumpWasPressed);
            saveData.data.Add("mBufferedUsableJump", mBufferedUsableJump);
            saveData.data.Add("mGrounded", mGrounded);
            saveData.data.Add("mCoyoteUsable", mCoyoteUsable);
            saveData.data.Add("mGrabbed", mGrabbed);
            saveData.data.Add("mFrameVelocity", mFrameVelocity);
			saveData.data.Add("mTime", mTime);

			print($"[SaveSystem] - Saving PlayerController {gameObject.name}");
            return saveData;
        }

        public void Load(SaveableObjectData saveableObject)
        {
	        print($"[SaveSystem] - Cargando PlayerController {saveableObject.objectID}");

	        transform.position = JsonConvert.DeserializeObject<Vector3>(saveableObject.data["position"].ToString());
	        transform.rotation = JsonConvert.DeserializeObject<Quaternion>(saveableObject.data["rotation"].ToString());
	        mFrameInput = JsonConvert.DeserializeObject<FrameInput>(saveableObject.data["mFrameInput"].ToString());
	        mFrameVelocity = JsonConvert.DeserializeObject<Vector2>(saveableObject.data["mFrameVelocity"].ToString());
	       
	        mGrounded = Convert.ToBoolean(saveableObject.data["mGrounded"]);
	        mJumpToConsume = Convert.ToBoolean(saveableObject.data["mJumpToConsume"]);
	        mCoyoteUsable = Convert.ToBoolean(saveableObject.data["mCoyoteUsable"]);
	        mBufferedUsableJump = Convert.ToBoolean(saveableObject.data["mBufferedUsableJump"]);
	        mTimeJumpWasPressed = Convert.ToSingle(saveableObject.data["mTimeJumpWasPressed"]);
	       
	        mGrabbed = Convert.ToBoolean(saveableObject.data["mGrabbed"]);
	        mTime = Convert.ToSingle(saveableObject.data["mTime"]);
            gameObject.SetActive(saveableObject.isActive);
        }

        #endregion


        private void Awake()
		{
			mRB = GetComponent<Rigidbody2D>();
			mCollider = GetComponent<CapsuleCollider2D>();

			mCamera = Camera.main;

			mCachedQueryStartsInColliders = Physics2D.queriesStartInColliders;
		}

        private void Start()
        {
            SaveSystem.Instance.RegisterSaveable(this);
        }

        private void OnDestroy()
        {
            SaveSystem.Instance.RemoveSaveable(this);
        }

        private void Update()
		{
			mTime += Time.deltaTime;
			InputManager();
		}

		private void InputManager()
		{
			mFrameInput = new FrameInput
			{
				JumpDown = Input.GetButtonDown("Jump"),
				JumpHeld = Input.GetButton("Jump"),
				MoveInput = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical")),

				GrappleDown = Input.GetButtonDown("Fire1"),
				GrappleUp = Input.GetButtonUp("Fire1"),
				GrappleHeld = Input.GetButton("Fire1"),
				GrappleRetractHeld = Input.GetKey(KeyCode.E),
				GrappleExtendHeld = Input.GetKey(KeyCode.Q)
			};

			if (mStats.SnapInput)
			{
				mFrameInput.MoveInput.x = Mathf.Abs(mFrameInput.MoveInput.x) < mStats.HorizontalDeadZoneThreshold ? 0 : Mathf.Sign(mFrameInput.MoveInput.x);
				mFrameInput.MoveInput.y = Mathf.Abs(mFrameInput.MoveInput.y) < mStats.VerticalDeadZoneThreshold ? 0 : Mathf.Sign(mFrameInput.MoveInput.y);
			}

			if (mFrameInput.JumpDown)
			{
				mJumpToConsume = true;
				mTimeJumpWasPressed += mTime;
			}

			if (mFrameInput.GrappleDown)
			{
				mGrappleToConsume = true;
			}

			if (mFrameInput.GrappleUp)
			{
				mToUngrapple = true;
			}

            if (Input.GetKeyDown(KeyCode.P))
            {
                SaveSystem.Instance.SaveGameData();
            }

            if (Input.GetKeyDown(KeyCode.O))
            {
				SaveSystem.Instance.LoadGameData();
            }

        }

        private void FixedUpdate()
		{
			HandleGrapple();

			if (!mGrabbed)
			{
				CheckCollisions();

				HandleJump();
				HandleGravity();

				HandleDirection();

				Move();
			}
			else
			{
				HandleGrappledMovement();
			}
		}

		private void CheckCollisions()
		{
			// Set raycasts does not starts inside of colliders, so this collider will not be returned by raycast.
			Physics2D.queriesStartInColliders = false;

			bool GroundHit = Physics2D.CapsuleCast(mCollider.bounds.center, mCollider.size, mCollider.direction, 0, Vector2.down, mStats.GrounderDistance, ~mStats.PlayerLayer); //~mStats.PlayerLayer means NOT that layer.
			bool CeillingHit = Physics2D.CapsuleCast(mCollider.bounds.center, mCollider.size, mCollider.direction, 0, Vector2.up, mStats.GrounderDistance, ~mStats.PlayerLayer);

			bool WallHitLeft = Physics2D.CapsuleCast(mCollider.bounds.center, mCollider.size, mCollider.direction, 0, Vector2.left, mStats.WallDistance, ~mStats.PlayerLayer);
			bool WallHitRight = Physics2D.CapsuleCast(mCollider.bounds.center, mCollider.size, mCollider.direction, 0, Vector2.right, mStats.WallDistance, ~mStats.PlayerLayer);

			mWallDirection = WallHitLeft ? -1 : WallHitRight ? 1 : 0;
			mTouchingWall = mWallDirection != 0;

			if (CeillingHit) mFrameVelocity.y = Mathf.Min(0, mFrameVelocity.y);

			if (!mGrounded && GroundHit)
			{
				mGrounded = true;
				mCoyoteUsable = true;
				mBufferedUsableJump = true;
				mEndedJumpEarly = false;
				GroundedChanged?.Invoke(true, Mathf.Abs(mFrameVelocity.y));
			}
			else if (mGrounded && !GroundHit)
			{
				mGrounded = false;
				mFrameLeftGround = mTime;
				GroundedChanged?.Invoke(false, 0);
			}

			Physics2D.queriesStartInColliders = mCachedQueryStartsInColliders;
		}

		private void HandleJump()
		{
			if (!mEndedJumpEarly && !mGrounded && !mFrameInput.JumpHeld && mRB.linearVelocity.y > 0) mEndedJumpEarly = true;

			if (!mJumpToConsume && !HasBufferedJump) return;

			if (mGrounded || CanUseCoyote || mTouchingWall)
				ExecuteJump();
			mJumpToConsume = false;
		}

		private void ExecuteJump()
		{
			mEndedJumpEarly = false;
			mBufferedUsableJump = false;
			mCoyoteUsable = false;
			mTimeJumpWasPressed = 0;

			if (mTouchingWall)
			{
				mFrameVelocity = new Vector2(-mWallDirection * mStats.WallJumpForce.x, mStats.WallJumpForce.y);
				WallJumped?.Invoke(mWallDirection);
			}
			else
			{
				mFrameVelocity.y = mStats.JumpForce;
				Jumped?.Invoke();
			}

		}

		private void HandleGrapple()
		{
			if (mGrappleToConsume)
			{
				mGrappleToConsume = false;

				Vector3 position = transform.position;
				Vector2 MousePosition = mCamera.ScreenToWorldPoint(Input.mousePosition);
				Vector2 Direction = (MousePosition - (Vector2)position).normalized;

				RaycastHit2D Hit = Physics2D.Raycast(position, Direction, mStats.GrappleRange, ~mStats.PlayerLayer);

				if (Hit.collider != null)
				{
					mGrabbed = true;
					GrapplingShooted?.Invoke(Hit.point);
				}
			}

			if (mToUngrapple)
			{
				mToUngrapple = false;

				if (mGrabbed)
				{
					mGrabbed = false;
					mEndedJumpEarly = false;
					mBufferedUsableJump = false;
					mCoyoteUsable = false;
					mFrameVelocity = mRB.linearVelocity;
					Ungrapple?.Invoke();
				}
			}
		}

		private void HandleDirection()
		{
			if (mFrameInput.MoveInput.x == 0)
			{
				float Deceleration = mGrounded ? mStats.GroundDeceleration : mStats.AirDeceleration;
				mFrameVelocity.x = Mathf.MoveTowards(mFrameVelocity.x, 0, Deceleration * Time.fixedDeltaTime);
			}
			else
			{
				mFrameVelocity.x = Mathf.MoveTowards(mFrameVelocity.x, mFrameInput.MoveInput.x * mStats.MaxSpeed,
					mStats.Acceleration * Time.fixedDeltaTime);
			}
		}

		private void HandleGravity()
		{
			if (mGrounded && mFrameVelocity.y <= 0f)
			{
				mFrameVelocity.y = mStats.GroundingForce;
			}
			else
			{
				float OnAirGravity = mStats.FallAcceleration;
				if (mEndedJumpEarly && mFrameVelocity.y > 0)
					OnAirGravity *= mStats.JumpEndEarlyGravityModifier;

				mFrameVelocity.y = Mathf.MoveTowards(mFrameVelocity.y, -mStats.MaxFallSpeed,
					OnAirGravity * Time.fixedDeltaTime);
			}
		}

		private void Move() => mRB.linearVelocity = mFrameVelocity;

		private void HandleGrappledMovement()
		{
			mRB.AddForceX(mFrameInput.MoveInput.x * mStats.GrappedSpeed * Time.fixedDeltaTime, ForceMode2D.Impulse);
			mRB.AddForceY(mStats.GrappedGroundingForce * Time.fixedDeltaTime, ForceMode2D.Impulse);
		}


#if UNITY_EDITOR
		private void OnValidate()
		{
			if (mStats == null) Debug.LogWarning("Assign the SO of Stats to the PlayerController!", this);
		}

#endif
    }

	/**
	 * Struct with input for the current frame.
	 */
	public struct FrameInput
	{
		public bool JumpDown;
		public bool JumpHeld;
		public Vector2 MoveInput;

		public bool GrappleDown;
		public bool GrappleUp;
		public bool GrappleHeld;
		public bool GrappleRetractHeld;
		public bool GrappleExtendHeld;
	}

	/**
	 * Interface of a PlayerController.
	 * Handles input and movement using events.
	 */
	public interface IPlayerController
	{
		/**
		 * Event called when the player become grounded or left it.
		 * @param {bool} isGrounded.
		 * @param {float} yVelocity.
		 */
		public event Action<bool, float> GroundedChanged;

		public event Action Jumped;

		/**
		 * @param {int} wallDirection -1 if left, 1 if right.
		 */
		public event Action<int> WallJumped;

		/**
		 * Graplling was shoot.
		 * @param {Vector2} Point where the hook grabbed.
		 */
		public event Action<Vector2> GrapplingShooted;

		public event Action Ungrapple;


		public FrameInput FrameInput { get; }
	}
}