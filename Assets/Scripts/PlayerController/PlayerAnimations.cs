using UnityEngine;
using UnityEngine.Serialization;
using SF = UnityEngine.SerializeField;

namespace Atherox.Player
{
	[RequireComponent(typeof(AudioSource))]
	public class PlayerAnimations : MonoBehaviour
	{
		[Header("Refs")] [SF] private Animator mAnim;
		[SF] private SpriteRenderer mSprite;

		[Header("Settings")] [SF, Range(1f, 3f)] private float mMaxIdleSpeed;

		[SF] private float mMaxTilt = 5f;
		[SF] private float mTiltSpeed = 20f;

		[Header("Particles")] [SF] private float mParticleScale = 0.5f;
		[SF] private ParticleSystem mJumpParticles;
		[SF] private ParticleSystem mLandParticles;
		[SF] private ParticleSystem mMoveParticles;

		[FormerlySerializedAs("mFootSteps")] [Header("AudioClips")] [SF] private AudioClip[] mGroundSteps;

		private AudioSource mSource;
		private IPlayerController mPlayer;
		private bool mGrounded;
		private ParticleSystem.MinMaxGradient mCurrentGradient;

		private static readonly int GroundedKey = Animator.StringToHash("Grounded");
		private static readonly int IdleSpeedKey = Animator.StringToHash("IdleSpeed");
		private static readonly int JumpKey = Animator.StringToHash("Jump");

		private void Awake()
		{
			mSource = GetComponent<AudioSource>();
			mPlayer = GetComponentInParent<IPlayerController>();
		}

		private void OnEnable()
		{
			mPlayer.Jumped += OnJumped;
			mPlayer.WallJumped += OnWallJumped;
			mPlayer.GroundedChanged += OnGroundedChanged;

			mMoveParticles.Play();
		}

		private void OnDisable()
		{
			mPlayer.Jumped -= OnJumped;
			mPlayer.WallJumped -= OnWallJumped;
			mPlayer.GroundedChanged -= OnGroundedChanged;

			mMoveParticles.Stop();
		}


		private void Update()
		{
			if (mPlayer == null) return;

			HandleSpriteFlip();

			HandleIdleSpeed();

			HandleCharacterTilt();
		}

		private void HandleSpriteFlip()
		{
			if (mPlayer.FrameInput.MoveInput.x != 0) mSprite.flipX = mPlayer.FrameInput.MoveInput.x < 0;
		}

		private void HandleIdleSpeed()
		{
			float InputStrength = Mathf.Abs(mPlayer.FrameInput.MoveInput.x);
			mAnim.SetFloat(IdleSpeedKey, Mathf.Lerp(1, mMaxIdleSpeed, InputStrength));
			mMoveParticles.transform.localScale = Vector3.MoveTowards(mMoveParticles.transform.localScale, Vector3.one * InputStrength * mParticleScale, 2 * Time.deltaTime);
		}

		private void HandleCharacterTilt()
		{
			Quaternion RunningTilt = mGrounded ? Quaternion.Euler(0, 0, mMaxTilt * mPlayer.FrameInput.MoveInput.x) : Quaternion.identity;
			mAnim.transform.up = Vector3.RotateTowards(mAnim.transform.up, RunningTilt * Vector2.up * mParticleScale, mTiltSpeed * Time.deltaTime, 0f);
		}

		private void OnJumped()
		{
			mAnim.SetTrigger(JumpKey);
			mAnim.ResetTrigger(GroundedKey);

			if (mGrounded)
			{
				mJumpParticles.Play();
			}
		}

		private void OnWallJumped(int aWallDirection)
		{
			mAnim.SetTrigger(JumpKey);
			mAnim.ResetTrigger(GroundedKey);

			mJumpParticles.Play();
		}

		private void OnGroundedChanged(bool aGrounded, float aYValue)
		{
			mGrounded = aGrounded;

			if (aGrounded)
			{
				mAnim.SetTrigger(GroundedKey);
				mSource.PlayOneShot(mGroundSteps[Random.Range(0, mGroundSteps.Length)]);
				mMoveParticles.Play();

				mLandParticles.transform.localScale = Vector3.one * Mathf.InverseLerp(0, 40, aYValue) * mParticleScale;
				mLandParticles.Play();
			}
			else
			{
				mMoveParticles.Stop();
			}
		}
	}
}