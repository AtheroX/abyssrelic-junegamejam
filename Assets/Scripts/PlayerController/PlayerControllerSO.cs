using UnityEngine;

namespace Atherox.Player.Info
{
	[CreateAssetMenu(fileName = "StatsSO", menuName = "AtheroX/StatsSO")]
	public class PlayerControllerSO : ScriptableObject
	{
	[Header("LAYERS")]
		[Tooltip("Layer the player is on")]
		public LayerMask PlayerLayer;

	[Header("INPUT")]
		[Tooltip("Makes all Input snap to an integer. Prevents gamepads from walking slowly. Recommended value is true to ensure gamepad/keybaord parity.")]
		public bool SnapInput = true;

		[Tooltip("Iinput required before you mount a ladder or climb a ledge. Avoids unwanted climbing using controllers"), Range(0.01f, 0.99f)]
		public float VerticalDeadZoneThreshold = 0.3f;

		[Tooltip("Minimum input required before a left or right is recognized. Avoids drifting with sticky controllers"), Range(0.01f, 0.99f)]
		public float HorizontalDeadZoneThreshold = 0.1f;

	[Header("MOVEMENT")]
		[Tooltip("The top horizontal movement speed")]
		public float MaxSpeed = 14;
		
		[Tooltip("The player's capacity to gain horizontal speed")]
		public float Acceleration = 120;

		[Tooltip("The pace at which the player comes to stop")]
		public float GroundDeceleration = 60;

		[Tooltip("The pace at which the player comes to stop whe is mid-air")]
		public float AirDeceleration = 30;

		[Tooltip("A constant downward force applied while grounded. Helps on slopes"), Range(0f, -10f)]
		public float GroundingForce = -1.5f;

		[Tooltip("The detection distance for grounding and roof detection"), Range(0f, 0.5f)]
		public float GrounderDistance = 0.05f;

		[Tooltip("The detection distance for Walljumping detection"), Range(0f, 0.5f)]
		public float WallDistance = 0.05f;

	[Header("JUMP")]
		[Tooltip("The immediate velocity applied when jumping")]
		public float JumpForce = 36;

		[Tooltip("The immediate velocity applied when jumping onto a wall")]
		public Vector2 WallJumpForce = new Vector2(30, 30);

		[Tooltip("The maximum vertical movement speed")]
		public float MaxFallSpeed = 40;

		[Tooltip("The player's capacity to gain fall speed. a.k.a. In Air Gravity")]
		public float FallAcceleration = 110;

		[Tooltip("The gravity multiplier added when jump is released early")]
		public float JumpEndEarlyGravityModifier = 3;

		[Tooltip("The time before coyote jump becomes unusable. Coyote jump allows jump to execute even after leaving a ledge")]
		public float CoyoteTime = .15f;

		[Tooltip("The amount of time we buffer a jump. This allows jump input before actually hitting the ground")]
		public float JumpBuffer = .2f;

		[Space]

	[Header("Grappling")]
		[Tooltip("Max distance for grapple")]
		public float GrappleRange = 10f;

		[Tooltip("Min distance for grapple")]
		public float GrappleMinDistance = 0.1f;

		[Tooltip("Max distance for grapple")]
		public float GrappleMaxDistance = 10f;

		[Tooltip("Speed to Retract the grapple")]
		public float GrappleRetractSpeed = 5f;

		[Tooltip("Speed to Extend the grapple")]
		public float GrappleExtendSpeed = 5f;

		[Tooltip("The speed of the player when grapped")]
		public float GrappedSpeed = 50;

		[Tooltip("The speed of the player when grapped")]
		public float GrappedGroundingForce = -50;

	}
}