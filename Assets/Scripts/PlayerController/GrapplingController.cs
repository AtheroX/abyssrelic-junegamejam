using Atherox.Editor;
using Atherox.Player.Info;
using UnityEngine;

namespace Atherox.Player
{
	public class GrapplingController : MonoBehaviour
	{
		[ExposeSO]
		[SerializeField] private PlayerControllerSO mStats;
		private IPlayerController mPlayer;

		private bool mIsGrappling;
		private Vector2 mGrapplePoint;
		private SpringJoint2D mSpringJoint;
		private LineRenderer mLineRenderer;
		
		private void Awake()
		{
			mPlayer = GetComponentInParent<IPlayerController>();

			mSpringJoint = gameObject.GetComponent<SpringJoint2D>();
			mSpringJoint.enabled = false;

			mLineRenderer = gameObject.GetComponent<LineRenderer>();
			mLineRenderer.startWidth = 0.3f;
			mLineRenderer.endWidth = 0.3f;
			mLineRenderer.enabled = false;
		}

		private void OnEnable()
		{
			mPlayer.GrapplingShooted += OnGrapplingShooted;
			mPlayer.Ungrapple += OnUngrapple;
		}

		private void OnDisable()
		{
			mPlayer.GrapplingShooted -= OnGrapplingShooted;
			mPlayer.Ungrapple -= OnUngrapple;
		}

		private void OnGrapplingShooted(Vector2 aPoint)
		{
			mGrapplePoint = aPoint;

			mSpringJoint.connectedAnchor = mGrapplePoint;
			mSpringJoint.distance = Vector2.Distance(transform.position, mGrapplePoint);
			mSpringJoint.enabled = true;
			mSpringJoint.autoConfigureDistance = false;
			mLineRenderer.enabled = true;
			mIsGrappling = true;
		}

		private void OnUngrapple()
		{
			mSpringJoint.enabled = false;
			mLineRenderer.enabled = false;
			mIsGrappling = false;
		}

		private void Update()
		{
			UpdateGrapple();
		}

		private void UpdateGrapple()
		{
			if (!mIsGrappling) return;

			mLineRenderer.SetPosition(0, transform.position);
			mLineRenderer.SetPosition(1, mGrapplePoint);

			if (mPlayer.FrameInput.GrappleRetractHeld)
			{
				mSpringJoint.distance = Mathf.Max(mSpringJoint.distance - mStats.GrappleRetractSpeed * Time.deltaTime, mStats.GrappleMinDistance);
			}
			else if (mPlayer.FrameInput.GrappleExtendHeld)
			{
				mSpringJoint.distance = Mathf.Min(mSpringJoint.distance + mStats.GrappleExtendSpeed * Time.deltaTime, mStats.GrappleMaxDistance);
			}
		}

#if UNITY_EDITOR
		private void OnValidate()
		{
			if (mStats == null) Debug.LogWarning("Assign the SO of Stats to the PlayerController!", this);
		}
#endif
	}
}