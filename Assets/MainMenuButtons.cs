using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuButtons : MonoBehaviour
{
    public void LoadSavedGame()
    {
        SaveSystem.Instance.LoadGameData();
    }
    
    public void NewGame()
    {
        SceneManager.LoadScene("Game", LoadSceneMode.Single);
    }
}
